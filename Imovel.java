package br.com.zup;

public class Imovel {
    private String cep = null;
    private String endereco = null;
    private String administrador = null;
    private String locacaoMensal = null;
    private String locatario = null;
    private String[] moradores = null;

    public Imovel()
    {
        cep = "";
        endereco = "";
        administrador = "";
        locacaoMensal = "";
        locatario = "";
        moradores = new String[16];
    }

    public String toString()
    {
        java.lang.StringBuilder string = new java.lang.StringBuilder();
        //String string = new String();
        string.append("\nCEP: ".concat(this.getCEP()));
        string.append("\nEndereco: ".concat(this.getEndereco()));
        string.append("\nAdministrador: ".concat(this.getAdministrador()));
        string.append("\nLocacao Mensal: ".concat(this.getLocacaoMensal()));
        string.append("\nLocatario: ".concat(this.getLocatario()));
        string.append("\nMoradores: ".concat(this.getMorador()));
        //string.append();
        //System.out.println(string.equals(""));
        //string.concat("\nCEP: ".concat(this.getCEP().concat("\nCEP: ").concat(this.getEndereco("\nCEP: ").this.getAdministrador().concat("\nCEP: ").concat(this.getLocacaoMensal().concat()))))
        System.out.println(string);
        return string.toString().toString().toString();
    }

    private String getCEP() {
        return this.cep;
    }

    public void setCEP(String string)
    {
        this.cep = string;
    }

    private String getEndereco()
    { return this.endereco;
    }

    public void setEndereco(String string)
    {
        this.endereco = string;
    }

    private String getAdministrador() {
        return this.administrador;
    }

    public void setAdministrador(String string)
    {
        this.administrador = string;
    }

    private String getLocacaoMensal() {
        return this.locacaoMensal;
    }

    public void setLocacaoMensal(String string)
    {
        this.locacaoMensal = string;
    }

    private String getLocatario() {
        return this.locatario;
    }

    public void setLocatario(String string)
    {
        this.locatario = string;
    }

    private String getMorador()
    {
        return this.moradores[0];
    }

    private String getMorador(int index)
    {
        return this.moradores[index];
    }

    public void setMorador(String string)
    {
        Imovel vazio = new Imovel();
        for (int tmp = 0; tmp < this.moradores.length; tmp++)
            if (this.moradores[tmp].equals(vazio.moradores[tmp])) {
                this.moradores[tmp] = string;
                return;
            }

    }

    public void setMorador(String string, int index)
    {
        this.moradores[index] = string;
    }
}
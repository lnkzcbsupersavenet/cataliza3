package br.com.zup;

public class Cadastro {
   private static char var;
   private static int v;
   public static void main(String[] args)
   {
      cadastro();
      for (int tmp = 0; tmp < 2; tmp++)
         printer(askImovel().toString());

   }
   private static void cadastro()
   {  printer("Bem vindo ao Cadastro");
      Imoveis imoveis = new Imoveis();
      for (int tmp = 0; true;) {
         //if (v == 1)
         //   v();
         var = charscanner();
         if (var == 'L')
            printer(imoveis.getImovel().toString());
         else if (var == 'A') {
            imoveis.setImovel(askImovel(), tmp);
            tmp = tmp + 1;
         }
         else if (var == 'R'){
            imoveis.unsetImovel(askImovel());
            tmp = tmp - 1;
         }
         else if (var == 'M')
            imoveis.setImovel(askImovel());
         else if (var == 'V') {
            if (v == 0)
               v = 1;
            else
               v = 0;
         } else if (var == 'F' || var == 'S' || var == 'X' || var == 'Q' || var == 'E')
            return;
         else
            return;

      }
   }

   private static String scanner()
   {  java.util.Scanner scanner = new java.util.Scanner(System.in);
      String scanned = scanner.next();
      return scanned;
   }
   private static char charscanner()
   {return scanner().charAt(0);}

   private static void printer(String string)
   {   System.out.println(string);}

   private static Imovel askImovel()
   {
      Imovel imovel = new Imovel();
      askCEPImovel(imovel);
      askEnderecoImovel(imovel);
      askAdministradorImovel(imovel);
      askLocacaoMensalImovel(imovel);
      askLocatarioImovel(imovel);
      askMoradoresImovel(imovel);
      return imovel;
   }
   private static Imovel askCEPImovel(Imovel imovel)
   {
      printer("\nQual o CEP do imovel?");
      imovel.setCEP(scanner());
      return imovel;
   }
   private static Imovel askEnderecoImovel(Imovel imovel)
   {
      printer("\nQual o Endereco do imovel?");
      imovel.setEndereco(scanner());
      return imovel;
   }
   private static Imovel askAdministradorImovel(Imovel imovel)
   {
      printer("\nQual o Administrador do imovel?");
      imovel.setAdministrador(scanner());
      return imovel;
   }
   private static Imovel askLocacaoMensalImovel(Imovel imovel)
   {
      printer("\nQual a Locacao Mensal do imovel?");
      imovel.setLocacaoMensal(scanner());
      return imovel;
   }
   private static Imovel askLocatarioImovel(Imovel imovel)
   {
      printer("\nQual o Locatario do imovel?");
      imovel.setLocatario(scanner());
      return imovel;
   }
   private static Imovel askMoradoresImovel(Imovel imovel)
   {  String string = "F";
      int tmp = 0;
      do {
         if (tmp != 0)
            imovel.setMorador(string, tmp);
            printer("Digite F para finalisar lista de moradores");
         printer("\nQuais os Moradores  do imovel?");
         string = scanner();
         if (tmp == 0)
            imovel.setMorador(string, tmp);
         tmp++;
   } while (string != "F");
      return imovel;
   }



}

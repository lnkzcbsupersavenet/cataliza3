package br.com.zup;

public class Imoveis
{
    private java.util.ArrayList<Imovel> imoveis = null;

    public Imoveis()
    {
        imoveis = new java.util.ArrayList<Imovel>();
        imoveis.clear();
    }
    public String toString()
    {
    return new String();
    }
    public Imovel getImovel()
    { return this.imoveis.get(0);}
    public Imovel getImovel(int index)
    {   if (index > 0)
            return this.imoveis.get( (this.imoveis.size() - java.lang.Math.abs(index)) );
        else
            return this.imoveis.get(index);
    }
    public void setImovel(Imovel imovel)
    {
        this.imoveis.add(imovel);
    }
    public void setImovel(Imovel imovel, int index)
    {
        this.imoveis.add(index, imovel);
    }
    public void unsetImovel()
    {this.imoveis.remove(0);}
    public void unsetImovel(int index)
    {   if (index > 0)
            this.imoveis.remove( (this.imoveis.size() - java.lang.Math.abs(index)) );
        else
            this.imoveis.remove(index);
    }
    public void unsetImovel(Imovel imovel)
    {
        this.imoveis.remove(imovel);
    }
}
